import Company from './company';
import Department from './department';
export default class Routes{
	constructor(app) {
		this.app = app;
	}

	createServer(){			
		this.app.get('/', (req, res) => {
			res.send("hi");
		});

		new Company(this.app).service();
		new Department(this.app).service();
	}
}