
exports.up = async function(knex, Promise) {
  try{let department = await knex.schema.createTable('department',(table)=>{
			table.increments('id').primary();
			  table.string('name').notNullable();
			  table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
			  table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));
		});
		return department;
	}catch(r){
		console.log(e);
	}
};

exports.down = async function(knex, Promise) {
  try{let department = await knex.schema.dropTableIfExists('department');
    	return department;
	}catch(e){
		console.log(e);
	}
};
