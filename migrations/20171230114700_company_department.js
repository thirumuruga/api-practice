exports.up = async function(knex, Promise) {
  try{let company_department = await knex.schema.createTable('company_department',(table)=>{
			table.increments('id').primary();
			table.integer('company').references('company.id')
			table.integer("department").references('department.id');
		});
		return company_department;
	}catch(r){
		console.log(e);
	}
};

exports.down = async function(knex, Promise) {
  try{let company_department = await knex.schema.dropTableIfExists('company_department');
    	return company_department;
	}catch(e){
		console.log(e);
	}
};

