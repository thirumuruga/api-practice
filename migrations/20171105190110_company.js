
exports.up = async function(knex, Promise) {
	try{let company=await knex.schema.createTable('company',(table)=>{
			table.increments('id').primary();
			  table.string('name').notNullable();
			  table.string('address').notNullable();
			  table.timestamp('created_at').notNullable().defaultTo(knex.raw('now()'));
			  table.timestamp('updated_at').notNullable().defaultTo(knex.raw('now()'));
		});
		return company;
	}catch(r){
		console.log(e);
	}
};

exports.down = async function(knex, Promise) {
  try{let company = await knex.schema.dropTableIfExists('company');
    return company;
	}catch(e){
		console.log(e);
	}
};
