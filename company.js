import Connection from './database';
import Promise from 'bluebird';
export default class Company{
	constructor(app) {
		this.app = app;
	}

	service(){	
		
		this.app.post('/company',(req, res)=>{
			let {name,address,departments} =req.body
				Connection.transaction(query=>{
					return query.insert({name:name,address:address},'id').into('company').then((ids)=>{
						return Promise.map(departments,(department)=>{
							return query.insert({company:ids[0],department:department}).into('company_department');
						});
					});
				}).then(value=>{
					res.json(value);
				}).catch(e=>{
					res.error(e);
				});
		});

		this.app.get('/company',(req, res)=>{
			Connection('company').select().then(value=>{
				res.json(value);
			}).catch(e=>{
				res.error(e);
			});
		});

		this.app.put('/company/:id',(req, res)=>{
			let {name,address,departments} =req.body
			Connection.transaction(query=>{
					return query.update({name:name,address:address},'id').where('id','=',req.params.id).into('company').then((ids)=>{
						return query.del().from('company_department').where('company','=',req.params.id).then(id=>{
							return Promise.map(departments,(department)=>{		
								return query.insert({company:req.params.id,department:department}).into('company_department');
							});
						});
					});
				}).then(value=>{
					res.json(value);
				}).catch(e=>{
					res.error(e);
				});
		});

		this.app.get('/company/:id',(req, res)=>{
			Connection('company_department')
			.select('company.address','company.name as com_name','company.id as company_id','department.name as dep_name','department.id as dep_id',).where('company','=',req.params.id)
			.innerJoin('company','company_department.company','company.id')
			.innerJoin('department','company_department.department','department.id')
			.then(value=>{
				let company={
					id:value[0].company_id,	
					name:value[0].com_name,
					address:value[0].address,
					departments:value.map(v=>{return {id:v.dep_id,name:v.dep_name}})
				};
				res.json(company);
			}).catch(e=>{
				res.status(400).send(e);
			});
		});

		this.app.del('/company/:id',(req,res)=>{
				Connection.transaction(query=>{
					return query.del('*').from('company_department').where('company','=',req.params.id)
						.then(id=>{
							return query.del('*').from('company').where('id','=',req.params.id);
						});
				}).then(v=>{
					res.json(v);
				}).catch(e=>{
					res.status(400).send(e);
				});
		});
	}
}