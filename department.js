import Connection from './database';

export default class Department{
	constructor(app) {
		this.app = app;
	}

	service(){	
		
		this.app.post('/department',(req, res)=>{
			Connection('department').insert(req.body).returning('id').then(value=>{
				res.json(value);
			}).catch(e=>{
				res.status(400).send(e);
			});
		});
		this.app.get('/department',(req, res)=>{
			Connection('department').select().then(value=>{
				res.json(value);
			}).catch(e=>{
				res.status(400).send(e);
			});
		});

		this.app.put('/department/:id',(req, res)=>{
			Connection('department').where('id','=',req.params.id).update(req.body).then(value=>{
				res.json(value);
			}).catch(e=>{
				res.status(400).send(e);
			});
		});

		this.app.get('/department/:id',(req, res)=>{
			Connection('department').select().where('id','=',req.params.id).then(value=>{
				res.json(value);
			}).catch(e=>{
				res.status(400).send(e);
			});
		});

		this.app.del('/department/:id',(req,res)=>{
			Connection('department').where('id','=',req.params.id).del().then(v=>{
				res.json(v);
			}).catch(e=>{
				res.error(e);
			});
		});
		this.app.post('/mapping',(req,res)=>{		
			Connection('company_department').insert(req.body).returning('id').then(value=>{
					res.json(value);
				}).catch(e=>{
					res.status(400).send(e);
				});
		});
	}
}